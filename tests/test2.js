xdescribe('DuckDuckGo search', () => {
    it('searches for WebdriverIO', () => {
        browser.url('https://duckduckgo.com/')

        // $('#search_form_input_homepage').setValue('WebdriverIO')
        // $('#search_button_homepage').click()

        $('a.tag-home__link.js-tag-item-link').click();
        browser.pause(6000)
        const title = $('dt.talking-points__description-title').getText();
        console.log('Title is: ' + title)
        // outputs: "Title is: WebdriverIO (Software) at DuckDuckGo"
    })
})